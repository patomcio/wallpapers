default:
	@echo "run:"
	@echo "   make backup  -> to update the backup directory (bak/)"
	@echo "   make append  -> to add all images from toadd/ into wallpapers/"
	@echo "   make reorder -> reorder all wallpapers in the wallpapers/ directory"
	@echo "   make random  -> to select a random wallpaper"

backup:
	if [ -d bak ]; then rm -r bak; fi
	cp wallpapers/ bak/ -r

append:
	@if [ "$$(ls -A ./toadd | grep -v '^\.gitignore$$')" = "" ]; then \
	  echo "The toadd directory is empty. No files to process."; \
	else \
		val=$$(ls -l wallpapers | wc -l); \
		for file in ./toadd/*; do \
			if [ "$$(basename $$file)" != ".gitignore" ]; then \
				ffmpeg -i "$$file" "wallpapers/wall-$$val.jpg"; \
				rm "$$file"; \
				val=$$((val + 1)); \
			fi \
		done; \
	fi

reorder: backup
	mv wallpapers/* toadd/
	make append

random:
	@find wallpapers/ -type f | shuf -n 1

.PHONY: backup append reorder random
